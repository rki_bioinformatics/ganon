# ganon

ganon is a k-mer based read classification tool which uses Interleaved Bloom Filters in conjunction with a taxonomic clustering and a k-mer counting-filtering scheme. 

## Source code, development version and manual at:

https://github.com/pirovc/ganon/

### Quick installation

 [![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat)](http://bioconda.github.io/recipes/ganon/README.html)
 
```shh
conda install -c bioconda -c conda-forge ganon
```